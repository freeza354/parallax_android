package com.rf_4210171024.parallaxapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class GameView extends SurfaceView implements Runnable {

    ArrayList<Background> backgrounds;

    private Thread gameThread = null;
    volatile boolean running;

    private SurfaceHolder ourHolder;
    private Canvas canvas;
    private Paint paint;

    Context context;

    long fps = 60;
    int screenWidth, screenHeight;

    public GameView(Context context, int screenWidth, int screenHeight){

        super(context);

        this.context = context;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;

        ourHolder = getHolder();
        paint = new Paint();

        backgrounds = new ArrayList<>();

        backgrounds.add(new Background(
                this.context,
                screenWidth,
                screenHeight,
                "newbg",0,120,1
        ));

        backgrounds.add(new Background(
                this.context,
                screenWidth,
                screenHeight,
                "newmountain",30,75,20
        ));
        backgrounds.add(new Background(
                this.context,
                screenWidth,
                screenHeight,
                "newgrass",40,90,125
        ));
    }

    @Override
    public void run(){
        while (running){
            long startTimeFrame = System.currentTimeMillis();

            update();
            draw();

            long timeThisFrame = System.currentTimeMillis() - startTimeFrame;

            if (timeThisFrame >= 1){
                fps = 1000/timeThisFrame;
            }
        }
    }

    private void update(){
        for (Background bg : backgrounds){
            bg.update(fps);
        }
    }

    private void drawBackground(int pos){
        Background bg = backgrounds.get(pos);

        Rect fromRect1 = new Rect(0, 0, bg.width - bg.xClip, bg.height);
        Rect toRect1 =new Rect(bg.xClip, bg.startY, bg.width, bg.endY);

        Rect fromRect2 = new Rect(bg.width - bg.xClip, 0, bg.width, bg.height);
        Rect toRect2 =new Rect(0, bg.startY, bg.xClip, bg.endY);

        if (!bg.reversedFirst){
            canvas.drawBitmap(bg.bitmap, fromRect1, toRect1, paint);
            canvas.drawBitmap(bg.bitmapReversed, fromRect2, toRect2, paint);
        }else {
            canvas.drawBitmap(bg.bitmap, fromRect2, toRect2, paint);
            canvas.drawBitmap(bg.bitmapReversed, fromRect1, toRect1, paint);
        }
    }

    private void draw(){
        if (ourHolder.getSurface().isValid()){
            canvas = ourHolder.lockCanvas();
            canvas.drawColor(Color.argb(255,0,3,70));

            drawBackground(0);
            drawBackground(1);
            drawBackground(2);

            paint.setTextSize(80);
            paint.setColor(Color.argb(255,133,133,193));

            canvas.drawText("Reza Faiz", screenWidth / 3 + 350, screenHeight - 175, paint);
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void pause(){
        running = false;

        try {
            gameThread.join();
        } catch (InterruptedException e){
            Log.e(GameView.class.getSimpleName(), "Error : " + e);
        }
    }

    public void resume(){
        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}
